Nginx image for Wordpress
=========================

Nginx container for Wordpress applications. Wordpress is not installed in the Image.

Document Root
-------------

The required document root for laravel is set to `<your wordpress project>`.

More information
----------------

For environment variables and configuration options check the
[readme of dockerwest nginx](https://github.com/dockerwest/nginx/blob/master/README.md)

Versions
--------

The following versions are available:
- stable: last stable version of nginx
- mainline: last mainline version of nginx

License
-------

MIT License (MIT). See [License File](LICENSE.md) for more information.
